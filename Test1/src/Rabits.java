
public class Rabits {

// -----------
	// PROPERTIES
	String x;
	String y;
	// -----------
	private int xPosition;
	private int yPosition;
	
 // -----------
	// CONSTRUCTOR 
	public Rabits(String x,String y) {
		this.x = x;
		this.y = y;
	}
	// ------------
	
	
	// -----------
	// METHODS 
	// ------------
	public void printCurrentPosition() {
		System.out.println("The current position of the rabbit is: ");
	}

	public void sayHello() {
		System.out.println("Hello! I am a rabbit!");
	}


	public String getX() {
		return x;
	}


	public void setX(String x) {
		this.x = x;
	}


	public String getY() {
		return y;
	}


	public void setY(String y) {
		this.y = y;
	}


	public int getxPosition() {
		return xPosition;
	}


	public void setxPosition(int xPosition) {
		this.xPosition = xPosition;
	}


	public int getyPosition() {
		return yPosition;
	}


	public void setyPosition(int yPosition) {
		this.yPosition = yPosition;
	}
	
	// ----------------
	// ACCESSOR METHODS
	
	// ----------------
	
	// Put all your accessor methods in this section.
	
	





	public static void main(String[] args) throws InterruptedException {
		Rabits rabbit = new Rabits("right","right");
		rabbit.sayHello();
		
		
		boolean runForever = true;

		while (runForever == true) {
			System.out.println("Carrot");
			Thread.sleep(3000);
		}
	}

}





